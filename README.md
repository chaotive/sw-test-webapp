#STAR WARS Starships

A simple data viewer for starships, defined as https://swapi.co/

## Development Features

- **Standard project bootstrapping**: This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using latest available version.
- **Automatic generation of local API declarations**: It can generate api Typescript declaration types by running generateTypes script. This way, api description can be kept updated and type-safe.
- **Unit tests**: using jest, following react community style recommendations: every file has an additional .test.extension name.
- **Snapshot testing**: has been added to allow for better testing of react components changes.
- **UI library**: Bootstrap 4 has been used for UI components.
- **Version control**: Project repository is hosted on Bitbucket, using git.
- **Automatic deployment**: It uses Bitbucket Pipelines for automatic deployment process.
- **Code linting**: It uses mostly tslint code linter rules, but also relies on some of eslint that didn't work from tslint as is. tslint is preferred though.

## Deployment

Npm scripts tasks has been defined to allow for manual and automatic deployment process.

At this time, human friendly versioning has not been set up (i.e: semantic versioning, like 1.0.0 and so on).

### Manual Deployment Process

You should run the following commands:

1. npm install
2. npm test
3. npm run build
4. npm run deploy

### Automatic Deployment Process

Everytime a commit to master branch is done, it will automatically run all the steps described on the Manual Deployment Process, using predefined Bitbucket repository variables to deploy into http://chaotive.cl/other/sw-test-webapp/

**Note: Concepts used for automatic deployment:** As current project version uses Bitbucket, there is a Bitbucket Pipeline set up to do automatic deployment on every commit to master branch.

This means that if somebody is doing experimental work, or a new feature, that work should be done on a branch and it won't be deployed until that branch is merged back to master. Master branch is protected on repository to only accept commits trough pull requests merging, making it compatible with code reviews before going into production, making sure that every new version is production ready (on current terms).

This way, current deployment is working on a very simple *Continous Delivery style*.<br>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `npm run generateTypes`

Generates Typescript declaration files on typings directory. It does it by reading swapi schema declaration for each endpoint it has. More declaration schemas can be added by adding more lines like this one on scripts/generateTypes.js file, and using the endpoint name as parameter (starships, in this case):

`generate('starships')`

**Note**: For some reason, the order of the generated parameters is not always the same. 

### `npm run deploy`

If project was previously built locally (meaning, *build* directory contains deployable version of it), then, it will upload its contents, using a very simple ftp connection to whatever values are present on the following environment variables:

1. FTPUSER: username to authenticate on connection
2. FTPPASSWORD: password to authenticate on connection
3. FTPHOST: host to authenticate and upload contents
4. FTPREMOTEROOT: remote directory to upload files directly. I.e: /other/sw-test-webapp/

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
