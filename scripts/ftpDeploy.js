const FtpDeploy = require('ftp-deploy')
const ftpDeploy = new FtpDeploy()

const config = {
  user: process.env.FTPUSER,
  password: process.env.FTPPASSWORD,
  host: process.env.FTPHOST,
  port: 21,
  localRoot: 'build',
  remoteRoot: process.env.FTPREMOTEROOT,
  include: ['*', '**/*'],
  deleteRemote: true,
  forcePasv: true
}

ftpDeploy.deploy(config)
  .then(res => console.log('finished:', res))
  .catch(err => console.log(err))
